FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get install -y --no-install-recommends knot-resolver && \
    apt-get clean && rm -rf /var/lib/apt/lists/*

COPY kresd.conf /etc/knot-resolver/kresd.conf

WORKDIR /var/cache/knot-resolver/
VOLUME ["/var/cache/knot-resolver", "/var/lib/knot-resolver"]

ENTRYPOINT ["/usr/sbin/kresd"]
CMD ["-n", "-c", "/etc/knot-resolver/kresd.conf"]
